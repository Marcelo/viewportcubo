package org.jmtipane;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;



/**
 * ViewportCubo.java <BR>
 * Taller 4_Viewport Cubo
 * author: Marcelo Tip�n
 * 09/08/2020
 * Dibuja un cubo y lo coloca en dos viewport de distintos tama�os
 */
public class ViewportCubo implements GLEventListener, KeyListener {

    //variables de Opengl
    static GL gl;
    static GLU glu;
    static GLUT glut;
    static float ang;
    static float ancho, alto;
    GLUquadric quad;
    
    //variables de traslaci�n y rotaci�n
    static float angx=0, angy=0, angz=0, cenx=0, ceny=0;
    static  float tx=0,ty=0, tz=0 ,sx=1,sy=1,rx=0,ry=0, rz=0;
    
    /**
     * Crea una ventana y la muestra
     * @param args 
     */
    public static void main(String[] args) {
        Frame frame = new Frame("Viewport_Cubo");
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new ViewportCubo());
        frame.add(canvas);
        frame.setSize(1000, 1000);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
        //permite controlar las teclas en el fram
        frame.addKeyListener(new ViewportCubo());
    }

    /**
     * Inicializa variables y color de la pantalla
     * @param drawable 
     */
    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    /**
     * Define la forma, perspectiva y dimensiones de la pantalla
     * @param drawable
     * @param x
     * @param y
     * @param width
     * @param height 
     */
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        ancho = width;
        alto = height;
        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        
//        glu.gluPerspective(45.0f, h, 1.0, 20.0);
//        gl.glOrtho(-200, 200, -200, 200, -200, 200);
        
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    /**
     * Se dibuja un cubo en dos viewport diferentes
     * @param drawable 
     */
    public void display(GLAutoDrawable drawable) {
        gl = drawable.getGL();
        glu = new GLU();
        glut = new GLUT();
        quad = glu.gluNewQuadric();
        
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();

        //permite usar transparencias
        gl.glEnable(GL.GL_DEPTH_TEST);

        //viewport 1
        //Dibuja las lineas de un cubo
        gl.glViewport(0, 0, (int)ancho/4, (int)alto/4);
        gl.glLoadIdentity();
        gl.glOrtho(-80, 80, -80, 80, -600, 0);
        gl.glColor3f(1, 0, 0);
            //Dibuja el fondo
            gl.glBegin(GL.GL_QUADS);
            gl.glVertex3f(-80, -80, 501);
            gl.glVertex3f(80, -80, 501);
            gl.glVertex3f(80, 80, 501);
            gl.glVertex3f(-80, 80, 501);
            gl.glEnd();
        gl.glTranslatef(0, 0, 550);
        gl.glScalef(1, 1, 1);
        gl.glRotatef(45, 1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 1, 1);
        glut.glutWireCube(60);
            
        
        
        //viewport 2
        //Dibuja las lineas de un cubo
        gl.glViewport(0, 0, (int)ancho, (int)alto);
        gl.glLoadIdentity();
        gl.glOrtho(-200, 200, -200, 200, -200, 200);
        gl.glTranslatef(tx, ty, tz);
//        gl.glScalef(1, 1, 1);
        gl.glRotatef(45, 1, 1, 1);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotated(angy, 0, 1, 0);
        gl.glRotated(angz, 0, 0, 1);
        gl.glColor3f(1, 1, 1);
        glut.glutSolidCube(100);
        gl.glColor3f(1, 0, 0);
        glut.glutWireCube(101);

        
        
        // Flush all drawing operations to the graphics card
        gl.glFlush();
    }

    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }

    public void keyTyped(KeyEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Define las teclas para dar movimiento al cubo
     * @param e 
     */
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
        tx+=20f;
    }
        if(e.getKeyCode() == KeyEvent.VK_LEFT){
        tx-=20f;
        }
        if(e.getKeyCode() == KeyEvent.VK_UP){
        ty+=20f;
        }
        if(e.getKeyCode() == KeyEvent.VK_DOWN){
        ty-=20f;}
        
        if(e.getKeyCode() == KeyEvent.VK_1){
        tz-=20f;
        }
        if(e.getKeyCode() == KeyEvent.VK_2){
        tz+=20f;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_J){
        rx=1f;
        angx=angx+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_L){
        rx=1f;
        angx=angx-5;
        }
        if(e.getKeyCode() == KeyEvent.VK_I){
        ry=1f;
        angy=angy+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_K){
        ry=1f;
        angy=angy-5;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_N){
        rz=1f;
        angz=angz+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_M){
        rz=1f;
        angz=angz-5;
        }
        
        if(e.getKeyCode() == KeyEvent.VK_I){
        ry=1f;
        angy=angy+5;
        }
        if(e.getKeyCode() == KeyEvent.VK_K){
        ry=1f;
        angy=angy-5;
        }
        if(e.getKeyCode() == KeyEvent.VK_R){
        angx=0; angy=0; angz=0; cenx=0; ceny=0;
        tx=0;ty=0; sx=1; sy=1; rx=0; ry=0; rz=0;
        }
    }

    public void keyReleased(KeyEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

