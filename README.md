# Viewportcubo

Crea un cubo, haciendo uso de librerías glut, y lo coloca en dos viewport distintos.

Este proyecto contiene 2 carpetas: 

1. La primera carpeta contiene imágenes referenciales del programa.
    ![imagen1](https://framagit.org/Marcelo/viewportcubo/-/raw/master/Im%C3%A1genes/viewport%20cubo.png)
2. La segunda carpeta contiene los archivos correspondientes al programa:
    

- build
- nbproject
- src/org/jmtipane
- build.xml
- manifest.mf